'use strict';
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
dotenv.config();


/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    const roles = await queryInterface.sequelize.query(
      `SELECT id FROM roles WHERE value = 'admin';`
    );
    const hashedPassword = await bcrypt.hash(process.env.ADMIN_PASSWORD, 10);

    const adminRoleId = roles[0][0].id;

    const [userId] = await queryInterface.bulkInsert('users', [
      {
        email: 'admin@example.com',
        password: hashedPassword,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ],
      { returning: ['id'] });

    await queryInterface.bulkInsert('user_role', [
      {
        userId: userId.id,
        roleId: adminRoleId,
      },
    ], {});
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    const users = await queryInterface.sequelize.query(
      `SELECT id FROM users WHERE email = 'admin@example.com';`
    );
    const adminUserId = users[0][0].id;

    // Удаляем связи пользователя с ролями
    await queryInterface.bulkDelete('user_role', { userId: adminUserId }, {});

    // Удаляем пользователя
    await queryInterface.bulkDelete('users', { email: 'admin@example.com' }, {});
  },
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
};
