import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule } from '@nestjs/config';
import { UsersModule } from './users/users.module';
import { User } from './users/users.model';
import { RolesModule } from './roles/roles.module';
import { Role } from './roles/roles.model';
import { UserRole } from './roles/user-role.model';
import { AuthModule } from './auth/auth.module';
import { JwtAuthGuard } from './auth/jwt-auth.guard';
import { OtpPasswordModule } from './otp-password/otp-password.module';
import { OtpPassword } from './otp-password/otp-password.model';
import { MailerModule } from '@nestjs-modules/mailer';
import { OtpPasswordMailer } from './otp-password/otpMailer';

@Module({
  controllers: [],
  providers: [],
  imports: [
    MailerModule.forRoot({
      transport: {
        host: '0.0.0.0',
        port: 1025,
        ignoreTLS: true,
      },
    }),
    ConfigModule.forRoot({
      envFilePath: '.env',
    }),
    SequelizeModule.forRoot({
      dialect: 'postgres',
      host: 'localhost',
      port: parseInt(process.env.DB_PORT),
      username: process.env.BD_USERNAME,
      password: process.env.BD_PASSWORD,
      database: process.env.DATABASE,
      models: [User, Role, UserRole, OtpPassword],
      // autoLoadModels: false,
    }),
    UsersModule,
    RolesModule,
    AuthModule,
    OtpPasswordModule,
  ],
})
export class AppModule {}
