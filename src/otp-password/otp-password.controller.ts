import { Body, Controller, Post, Request, UseGuards } from '@nestjs/common';
import { OtpPasswordService } from './otp-password.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Roles } from '../auth/roles.auht.decorator';
import { RolesGuard } from '../auth/roles.guard';
import { JwtAuthGuard } from '../auth/jwt-auth.guard';

@ApiTags('OtpPassword')
@Controller('otp-password')
export class OtpPasswordController {
  constructor(private readonly otpPasswordService: OtpPasswordService) {}

  @ApiOperation({ summary: 'Verify OTP Password' })
  @ApiResponse({ status: 200, type: Boolean })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Roles('admin', 'candidate')
  @Post('/confirmation')
  async confirmationOtp(
    @Request() req: any,
    @Body() reqOtp: string,
  ): Promise<any> {
    return this.otpPasswordService.updateRoleWhenAddOtp(req, reqOtp);
  }
}
