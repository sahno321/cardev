import { Module } from '@nestjs/common';
import { OtpPasswordController } from './otp-password.controller';
import { OtpPasswordService } from './otp-password.service';
import { SequelizeModule } from '@nestjs/sequelize';
import { OtpPassword } from './otp-password.model';
import { AuthModule } from '../auth/auth.module';
import { UsersModule } from '../users/users.module';
import { RolesModule } from '../roles/roles.module';

@Module({
  imports: [
    UsersModule,
    AuthModule,
    RolesModule,
    SequelizeModule.forFeature([OtpPassword]),
  ],
  controllers: [OtpPasswordController],
  providers: [OtpPasswordService],
  exports: [OtpPasswordService],
})
export class OtpPasswordModule {}
