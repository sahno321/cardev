import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { OtpPassword } from './otp-password.model';
import { CreatOtpPasswordDto } from './dto/create-otp-password.dto';
import { UsersService } from '../users/users.service';
import { RolesService } from '../roles/roles.service';

const roleValue = 'user';

@Injectable()
export class OtpPasswordService {
  constructor(
    @InjectModel(OtpPassword) private otpPasswordRepository: typeof OtpPassword,
    private userService: UsersService,
    private roleService: RolesService,
  ) {}

  async confirmationOtp(us: any, reqOtp: string): Promise<boolean> {
    const user = await this.userService.getUserByEmail(us);
    const otp = user.otpPassword.otp;
    const reqOtpNum: string = Object.keys(reqOtp)
      .map((key) => reqOtp[key])
      .join('');

    if (!otp || otp !== reqOtpNum) {
      throw new NotFoundException('Invalid OTP');
    }

    const expiresAtTimestamp = user.otpPassword.expiresAt;
    const currentTimestamp = new Date().getTime();

    if (currentTimestamp > expiresAtTimestamp) {
      throw new NotFoundException('OTP expired');
    }
    return true;
  }

  async updateRoleWhenAddOtp(req: any, reqOtp: string): Promise<boolean> {
    const user = await this.userService.getUserByEmail(req.user.email);
    const otpConfirmed = await this.confirmationOtp(req.user.email, reqOtp);

    if (otpConfirmed) {
      const roleId = await this.roleService.getRoleByValue(roleValue);
      await this.roleService.updateUserRole(req.user.id, roleId.id);
      await this.otpPasswordRepository.destroy({
        where: { id: user.otpPassword.id },
      });

      return true;
    }

    return false;
  }

  async verifyOtp(email: string) {
    const user = await this.userService.getUserByEmail(email);

    if (!user.otpPassword) {
      return this.createOtpPassword(user.id);
    } else {
      await this.otpPasswordRepository.destroy({
        where: { otp: user.otpPassword.otp },
      });
      return this.createOtpPassword(user.id);
    }
  }

  async createOtpPassword(userId: number): Promise<OtpPassword> {
    const otp = this.generateOtp();
    const expiresAt = this.calculateExpirationTime();
    const otpPasswordDto: CreatOtpPasswordDto = {
      userId,
      otp,
      expiresAt,
    };
    return await this.otpPasswordRepository.create(otpPasswordDto);
  }

  private calculateExpirationTime(): Date {
    const expirationTime = new Date();
    expirationTime.setHours(expirationTime.getHours() + 1);
    return expirationTime;
  }

  private generateOtp(): string {
    const digits = '0123456789';
    let otp = '';
    for (let i = 0; i < 4; i++) {
      const index = Math.floor(Math.random() * digits.length);
      otp += digits[index];
    }
    return otp;
  }
}
