import { IsNotEmpty, Length } from 'class-validator';

export class CreatOtpPasswordDto {
  @IsNotEmpty()
  @Length(4, 4)
  otp: string;

  @IsNotEmpty()
  userId: number;

  @IsNotEmpty()
  expiresAt: Date;
}
