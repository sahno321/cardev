import { Injectable, Logger } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import { CreateUserDto } from '../users/dto/create-user.dto';

@Injectable()
export class OtpPasswordMailer {
  private readonly logger = new Logger(OtpPasswordMailer.name);

  constructor(private readonly mailerService: MailerService) {}

  async sendEmailOtp(otp: string, userDto: CreateUserDto): Promise<void> {
    this.logger.log('Preparing to send email OTP');
    this.logger.debug(`OTP: ${otp}, Email: ${userDto.email}`);

    const mailOptions = {
      from: 'CaRRun@gmail.com',
      to: userDto.email,
      subject: 'Регистрация',
      text: `Здравствуйте, ${userDto.email}! Ваш пароль: ${otp}`,
    };

    try {
      await this.mailerService.sendMail(mailOptions);
      this.logger.log('Email sent successfully');
    } catch (error) {
      this.logger.error('Failed to send email', error.stack);
    }
  }
}
