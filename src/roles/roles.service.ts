import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Role } from './roles.model';
import { InjectModel } from '@nestjs/sequelize';
import { CreateRoleDto } from './dto/create-role.dto';
import { UserRole } from './user-role.model';

@Injectable()
export class RolesService {
  constructor(
    @InjectModel(Role) private roleRepository: typeof Role,
    @InjectModel(UserRole) private userRole: typeof UserRole,
  ) {}

  async createRole(dto: CreateRoleDto): Promise<Role> {
    const role = await this.roleRepository.findOne({
      where: { value: dto.value },
    });
    if (role) {
      throw new HttpException('Role already exists', HttpStatus.NOT_FOUND);
    }
    return await this.roleRepository.create(dto);
  }

  async getRoleByValue(value: string): Promise<Role> {
    return await this.roleRepository.findOne({ where: { value } });
  }

  async updateUserRole(userId: number, newRoleId: number): Promise<void> {
    await this.userRole.update({ roleId: newRoleId }, { where: { userId } });
  }
}
