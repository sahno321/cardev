import { Module } from '@nestjs/common';
import { RolesService } from './roles.service';
import { RolesController } from './roles.controller';
import { SequelizeModule } from '@nestjs/sequelize';
import { Role } from './roles.model';
import { User } from '../users/users.model';
import { UserRole } from './user-role.model';
import { AuthModule } from '../auth/auth.module';
import { UsersService } from '../users/users.service';

@Module({
  imports: [AuthModule, SequelizeModule.forFeature([Role, User, UserRole])],
  providers: [RolesService, UsersService],
  controllers: [RolesController],
  exports: [RolesService],
})
export class RolesModule {}
