import { IsEmail, IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({ example: 'user@gmail.com', description: 'Unique Email' })
  @IsEmail({}, { message: 'Incorrect email format' })
  @IsNotEmpty({ message: 'Mail required' })
  email: string;

  @ApiProperty({ example: '1234', description: 'Password min4 max16' })
  @IsNotEmpty({ message: 'Password required' })
  @Length(4, 16, { message: `Password must be between 4 and 16 characters` })
  password: string;
}
