import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from './users.model';
import { CreateUserDto } from './dto/create-user.dto';
import { RolesService } from '../roles/roles.service';

const roleNewUser = 'candidate';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User) private userRepository: typeof User,
    private roleService: RolesService,
  ) {}

  async createUser(dto: CreateUserDto) {
    const role = await this.roleService.getRoleByValue(roleNewUser);
    if (!role) {
      throw new HttpException('Role not found', HttpStatus.NOT_FOUND);
    } else {
      const user = await this.userRepository.create(dto);
      await user.$set('roles', [role.id]);
      return user;
    }
  }
  async updatePassword(user: User): Promise<User> {
    return await user.save();
  }

  async getAllUsers(): Promise<any> {
    return await this.userRepository.findAll({ attributes: ['id', 'email'] });
  }

  async getUserByEmail(email: string): Promise<any> {
    return await this.userRepository.findOne({
      where: { email },
      include: { all: true },
    });
  }
  // async updateUser(user: User) {
  //   return await this.userRepository.create(user);
  // }
}
