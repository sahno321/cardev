import {
  BelongsToMany,
  Column,
  DataType,
  HasOne,
  Model,
  Table,
} from 'sequelize-typescript';
import { Role } from '../roles/roles.model';
import { UserRole } from '../roles/user-role.model';
import { OtpPassword } from '../otp-password/otp-password.model';

interface CreateUserAttr {
  email: string;
  password: string;
}

@Table({ tableName: 'users' })
export class User extends Model<User, CreateUserAttr> {
  @Column({
    type: DataType.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @Column({ type: DataType.STRING, unique: true, allowNull: false })
  email: string;

  @Column({ type: DataType.STRING, unique: false, allowNull: false })
  password: string;

  @BelongsToMany(() => Role, () => UserRole)
  roles: Role[];

  @HasOne(() => OtpPassword)
  otpPassword: OtpPassword;
}
