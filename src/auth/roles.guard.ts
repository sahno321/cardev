import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
  ForbiddenException,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { ROLES_KEY } from './roles.auht.decorator';
import { UsersService } from '../users/users.service';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private readonly reflector: Reflector,
    private readonly usersService: UsersService,
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const roles = this.reflector.get<string[]>(ROLES_KEY, context.getHandler());
    if (!roles) {
      return true;
    }

    const req = context.switchToHttp().getRequest();
    const authHeader = req.headers.authorization;

    if (!authHeader) {
      throw new UnauthorizedException('Authorization header not found');
    }

    const [bearer, token] = authHeader.split(' ');

    if (bearer !== 'Bearer' || !token) {
      throw new UnauthorizedException('Invalid token format');
    }

    try {
      const decodedToken = this.jwtService.verify(token);
      const userEmail = decodedToken.email;
      const user = await this.usersService.getUserByEmail(userEmail);
      if (!user) {
        throw new UnauthorizedException('User not found');
      }

      req.user = user;

      const userRoles = user.roles.map((role) => role.value);

      if (!this.hasRole(userRoles, roles)) {
        throw new ForbiddenException('You do not have permission (roles)');
      }

      return true;
    } catch (e) {
      throw new UnauthorizedException('Invalid token');
    }
  }

  private hasRole(userRoles: string[], roles: string[]): boolean {
    return roles.some((role) => userRoles.includes(role));
  }
}
