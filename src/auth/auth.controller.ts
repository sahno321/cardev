import {
  Body,
  Controller,
  Param,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { RolesGuard } from './roles.guard';
import { Roles } from './roles.auht.decorator';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { JwtAuthGuard } from './jwt-auth.guard';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Login' })
  @ApiResponse({
    status: 200,
    description: 'Returns the authentication token',
    type: String,
  })
  @Post('/login')
  async login(@Body() userDto: CreateUserDto): Promise<any> {
    return await this.authService.login(userDto);
  }

  @ApiOperation({ summary: 'Registration' })
  @ApiResponse({
    status: 200,
    description: 'Returns the authentication token',
    type: String,
  })
  @Post('/registration')
  async registration(@Body() userDto: CreateUserDto): Promise<any> {
    return await this.authService.registration(userDto);
  }

  @ApiOperation({ summary: 'ForgotPassword' })
  @ApiResponse({
    status: 200,
    description: 'Returns massages',
    type: String,
  })
  @UseGuards(JwtAuthGuard)
  @Post('/forgot/password')
  async forgotPassword(@Request() req: any): Promise<any> {
    return await this.authService.forgotPassword(req);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/reset/password')
  async resetPassword(
    @Request() req: any,
    @Body() reqBody: ResetPasswordDto,
  ): Promise<any> {
    return await this.authService.resetPassword(req, reqBody);
  }
}
