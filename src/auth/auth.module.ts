import { forwardRef, Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { JwtModule } from '@nestjs/jwt';
import * as process from 'node:process';
import { OtpPasswordService } from '../otp-password/otp-password.service';
import { OtpPasswordMailer } from '../otp-password/otpMailer';
import { SequelizeModule } from '@nestjs/sequelize';
import { OtpPassword } from '../otp-password/otp-password.model';
import { Role } from '../roles/roles.model';
import { RolesService } from '../roles/roles.service';
import { UsersService } from '../users/users.service';
import { UserRole } from '../roles/user-role.model';
import { User } from '../users/users.model';

@Module({
  controllers: [AuthController],
  providers: [
    AuthService,
    OtpPasswordService,
    OtpPasswordMailer,
    RolesService,
    UsersService,
  ],
  imports: [
    SequelizeModule.forFeature([OtpPassword, UserRole, User, Role]),
    JwtModule.register({
      secret: process.env.JWT_PRIVATE_KEY || 'SECRET',
      signOptions: {
        expiresIn: '1d',
      },
    }),
  ],
  exports: [AuthService, JwtModule],
})
export class AuthModule {}
