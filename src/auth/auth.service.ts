import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { CreateUserDto } from '../users/dto/create-user.dto';
import { UsersService } from '../users/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { User } from '../users/users.model';
import { OtpPasswordService } from '../otp-password/otp-password.service';
import { OtpPasswordMailer } from '../otp-password/otpMailer';
import { ResetPasswordDto } from './dto/reset-password.dto';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    private otpPasswordService: OtpPasswordService,
    private otpMailer: OtpPasswordMailer,
  ) {}

  async login(userDto: CreateUserDto): Promise<any> {
    const candidate = await this.usersService.getUserByEmail(userDto.email);
    if (!candidate) {
      throw new HttpException(
        'Email not registration',
        HttpStatus.UNAUTHORIZED,
      );
    }
    const user = await this.validateUser(userDto);
    return this.generateToken(user);
  }

  async registration(userDto: CreateUserDto): Promise<any> {
    const candidate = await this.usersService.getUserByEmail(userDto.email);
    if (candidate) {
      throw new HttpException('Email already exists', HttpStatus.NOT_FOUND);
    }

    const hashedPassword = await bcrypt.hash(userDto.password, 10);
    const user = await this.usersService.createUser({
      ...userDto,
      password: hashedPassword,
    });

    const otp = await this.otpPasswordService.createOtpPassword(user.id);

    await this.otpMailer.sendEmailOtp(otp.otp, userDto);

    return this.generateToken(user);
  }
  async forgotPassword(req: any): Promise<any> {
    const user = await this.usersService.getUserByEmail(req.user.email);
    if (!user) {
      throw new HttpException(
        'User with this email does not exist',
        HttpStatus.NOT_FOUND,
      );
    }
    const otp = await this.otpPasswordService.verifyOtp(req.user.email);

    await this.otpMailer.sendEmailOtp(otp.otp, user);

    return { message: 'Password reset email sent' };
  }

  async resetPassword(req: any, reqBody: ResetPasswordDto): Promise<any> {
    const user = await this.usersService.getUserByEmail(req.user.email);
    if (!user) {
      throw new HttpException(
        'User with this email does not exist',
        HttpStatus.NOT_FOUND,
      );
    }

    const otpConfirmed = await this.otpPasswordService.confirmationOtp(
      req.user.email,
      reqBody.otp,
    );

    if (otpConfirmed) {
      user.password = await bcrypt.hash(reqBody.newPassword, 10);

      try {
        return await this.usersService.updatePassword(user);
      } catch (error) {
        throw new HttpException(
          'Error updating password',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }
    } else {
      throw new HttpException(
        'Invalid OTP or OTP expired',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }

  private async generateToken(user: User): Promise<any> {
    const payload = { email: user.email, id: user.id };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  private async validateUser(userDto: CreateUserDto): Promise<any> {
    const user = await this.usersService.getUserByEmail(userDto.email);
    const passwordEquals = await bcrypt.compare(
      userDto.password,
      user.password,
    );
    if (user && passwordEquals) {
      return user;
    }
    throw new UnauthorizedException('Invalid email or password');
  }
}
