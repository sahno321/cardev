import { IsNotEmpty, Length } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordDto {
  @ApiProperty({ example: '1234', description: 'Password min4 max16' })
  @IsNotEmpty({ message: 'Password required' })
  @Length(4, 16, { message: `Password must be between 4 and 16 characters` })
  newPassword: string;

  @ApiProperty({
    example: '1234',
    description: 'OTP password must be 4 digits ',
  })
  @IsNotEmpty()
  @Length(4, 4)
  otp: string;
}
