'use strict';

const { DataType} = require("sequelize-typescript");

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        unique: true,
        primaryKey: true
      },
      email: {
        type: DataType.STRING,
        unique: true,
        allowNull:false
      },
      password: {
        type: DataType.STRING,
        unique: false,
        allowNull:false
      },
      createdAt:{type:DataType.DATE,},
      updatedAt:{type:DataType.DATE},

    })
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.dropTable('users')
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
